﻿using System.Windows;

namespace HRSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow app = new MainWindow();
            ViewModel.ApplicationViewModel context = new ViewModel.ApplicationViewModel();
            app.DataContext = context;
            app.Show();
        }
    }
}
