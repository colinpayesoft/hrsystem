﻿namespace HRSystem
{
    public enum ApplicationPage
    {
        Dashboard = 0,
        Companies = 1,
        Employees = 2,
    }
}
