﻿namespace HRSystem.ViewModel
{
    public class CompaniesViewModel : BaseViewModel, IPageViewModel
    {
        public string Name
        {
            get
            {
                return "Company Details";
            }
        }
    }
}
