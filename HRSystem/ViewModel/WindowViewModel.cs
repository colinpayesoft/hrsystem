﻿using System.Windows;
using System.Windows.Input;

namespace HRSystem.ViewModel
{
    class WindowViewModel : BaseViewModel
    {
        #region Private Member

        private Window mWindow;
        private WindowResizer mWindowResizer;
        private Thickness mOuterMarginSize = new Thickness(5);
        private int mWindowRadius = 5;
        private WindowDockPosition mDockPosition = WindowDockPosition.Undocked;

        #endregion

        #region Public Properties

        public double WindowMinimumWidth { get; set; } = 1280;
        public double WindowMinimumHeight { get; set; } = 720;
        public bool BeingMoved { get; set; }
        public bool Borderless => (mWindow.WindowState == WindowState.Maximized || mDockPosition != WindowDockPosition.Undocked);
        public int ResizeBorder => mWindow.WindowState == WindowState.Maximized ? 0 : 4;
        public Thickness ResizeBorderThickness => new Thickness(OuterMarginSize.Left + ResizeBorder,
                                                                OuterMarginSize.Top + ResizeBorder,
                                                                OuterMarginSize.Right + ResizeBorder,
                                                                OuterMarginSize.Bottom + ResizeBorder);
        public Thickness InnerContentPadding { get; set; } = new Thickness(0);
        public Thickness OuterMarginSize
        {
            get => mWindow.WindowState == WindowState.Maximized ? mWindowResizer.CurrentMonitorMargin : (Borderless ? new Thickness(0) : mOuterMarginSize);
            set => mOuterMarginSize = value;
        }
        public int WindowRadius
        {
            get => Borderless ? 0 : mWindowRadius;
            set => mWindowRadius = value;
        }
        public int FlatBorderThickness => Borderless && mWindow.WindowState != WindowState.Maximized ? 1 : 0;
        public CornerRadius WindowCornerRadius => new CornerRadius(WindowRadius);
        public int TitleHeight { get; set; } = 42;
        public GridLength TitleHeightGridLength => new GridLength(TitleHeight + ResizeBorder);
        public bool DimmableOverlayVisible { get; set; }

        #endregion

        #region Commands 

        public ICommand MinimizeCommand { get; set; }
        public ICommand MaximizeCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand MenuCommand { get; set; }


        #endregion

        #region Constructor

        public WindowViewModel(Window window)
        {
            mWindow = window;

            // Listen out for the window resizing
            mWindow.StateChanged += (sender, e) =>
            {
                // Fire off events for all properties that are affected by a resize
                WindowResized();
            };

            //Create Commands
            MinimizeCommand = new RelayCommand(() => mWindow.WindowState = WindowState.Minimized);
            MaximizeCommand = new RelayCommand(() => mWindow.WindowState ^= WindowState.Maximized);
            CloseCommand = new RelayCommand(() => mWindow.Close());
            MenuCommand = new RelayCommand(() => SystemCommands.ShowSystemMenu(mWindow, GetMousePosition()));

            //Fix window resize issue
            mWindowResizer = new WindowResizer(mWindow);
            mWindowResizer.WindowDockChanged += (dock) =>
            {
                mDockPosition = dock;
                WindowResized();
            };
            mWindowResizer.WindowStartedMove += () =>
            {
                BeingMoved = true;
            };
            mWindowResizer.WindowFinishedMove += () =>
            {
                BeingMoved = false;
                if (mDockPosition == WindowDockPosition.Undocked && mWindow.Top == mWindowResizer.CurrentScreenSize.Top)
                    mWindow.Top = -OuterMarginSize.Top;
            };
        }

        #endregion

        #region Private Helpers

        private Point GetMousePosition()
        {
            return mWindowResizer.GetCursorPosition();
        }

        private void WindowResized()
        {
            // Fire off events for all properties that are affected by a resize
            OnPropertyChanged(nameof(Borderless));
            OnPropertyChanged(nameof(FlatBorderThickness));
            OnPropertyChanged(nameof(ResizeBorderThickness));
            OnPropertyChanged(nameof(OuterMarginSize));
            OnPropertyChanged(nameof(WindowRadius));
            OnPropertyChanged(nameof(WindowCornerRadius));
        }

        #endregion
    }
}
