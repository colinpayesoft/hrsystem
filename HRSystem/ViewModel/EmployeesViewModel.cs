﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ViewModel
{
    public class EmployeesViewModel : BaseViewModel, IPageViewModel
    {
        public string Name
        {
            get
            {
                return "Employee Details";
            }
        }
    }
}
