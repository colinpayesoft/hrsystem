﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRSystem.ViewModel
{
    public class DashboardViewModel : BaseViewModel, IPageViewModel
    {
        public string Name
        {
            get
            {
                return "Dashboard";
            }
        }
    }
}
