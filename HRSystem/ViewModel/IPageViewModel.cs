﻿namespace HRSystem.ViewModel
{
    public interface IPageViewModel
    {
        string Name { get; }
    }
}
