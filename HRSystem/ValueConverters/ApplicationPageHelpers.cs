﻿using HRSystem.ViewModel;
using System;
using System.Diagnostics;
using System.Globalization;

namespace HRSystem.ValueConverters
{
    public static class ApplicationPageHelpers
    {
        /// <summary>
        /// Takes a <see cref="ApplicationPage"/> and a view model, if any, and creates the desired page
        /// </summary>
        /// <param name="page"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static BasePage ToBasePage(this ApplicationPage page, object viewModel = null)
        {
            // Find the appropriate page
            switch (page)
            {
                case ApplicationPage.Dashboard:
                    return new DashboardPage(viewModel as DashboardViewModel);

                case ApplicationPage.Companies:
                    return new CompaniesPage(viewModel as CompaniesViewModel);

                case ApplicationPage.Employees:
                    return new EmployeesPage(viewModel as EmployeesViewModel);

                default:
                    Debugger.Break();
                    return null;
            }
        }

        /// <summary>
        /// Converts a <see cref="BasePage"/> to the specific <see cref="ApplicationPage"/> that is for that type of page
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static ApplicationPage ToApplicationPage(this BasePage page)
        {
            // Find application page that matches the base page
            if (page is DashboardPage)
                return ApplicationPage.Dashboard;

            if (page is CompaniesPage)
                return ApplicationPage.Companies;

            if (page is EmployeesPage)
                return ApplicationPage.Employees;

            // Alert developer of issue
            Debugger.Break();
            return default(ApplicationPage);
        }
    }
}
