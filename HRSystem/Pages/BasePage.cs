﻿using Dna;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HRSystem.Pages
{
    public class BasePage : UserControl
    {
        #region Private Member

        private object mViewModel;

        #endregion

        #region Public Properties

        public object ViewModelObject
        {
            get => mViewModel;
            set
            {
                if (mViewModel == value)
                    return;
                mViewModel = value;
                OnViewModelChanged();
                DataContext = mViewModel;
            }
        }

        #endregion

        //#region Constructor

        //public BasePage()
        //{
        //    Loaded += BasePage_LoadedAsync;
        //}

        //#endregion

        protected virtual void OnViewModelChanged()
        {

        }

    }

    public class BasePage<VM> : BasePage
        where VM : BaseViewModel, new()
    {
        #region Public Properties

        public VM ViewModel
        {
            get => (VM)ViewModelObject;
            set => ViewModelObject = value;
        }

        #endregion

        #region Constructor

        public BasePage() : base()
        {
            // If in design time mode...
            if (DesignerProperties.GetIsInDesignMode(this))
                // Just use a new instance of the VM
                ViewModel = new VM();
            else
                // Create a default view model
                ViewModel = Framework.Service<VM>() ?? new VM();
        }

        /// <summary>
        /// Constructor with specific view model
        /// </summary>
        /// <param name="specificViewModel">The specific view model to use, if any</param>
        public BasePage(VM specificViewModel = null) : base()
        {
            // Set specific view model
            if (specificViewModel != null)
                ViewModel = specificViewModel;
            else
            {
                // If in design time mode...
                if (DesignerProperties.GetIsInDesignMode(this))
                    // Just use a new instance of the VM
                    ViewModel = new VM();
                else
                {
                    // Create a default view model
                    ViewModel = Framework.Service<VM>() ?? new VM();
                }
            }
        }

        #endregion
    }
}
